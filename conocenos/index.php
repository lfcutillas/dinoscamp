<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Diana and Dino's Camp</title>
	<meta name="description" content="">
	<meta name="keywords" content="website template, css3, one page, bootstrap, app template, web app, start-up">
	<meta name="author" content="Pixel Buddha and PSD2HTML for Codrops">
	<link rel="shortcut icon" href="favicons/favicon.ico">
	<meta name="msapplication-TileColor" content="#603cba">
	<meta name="msapplication-TileImage" content="../favicons/mstile-144x144.png">
	<meta name="msapplication-config" content="../favicons/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	<link rel="stylesheet" href="../css/bootstrap.css">
	<link rel="stylesheet" href="../fonts/font-awesome-4.3.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="../css/all.css">
</head>
<body>
<div id="wrapper">
		<header id="header">
			<div class="container">
				<nav id="nav">
					<div class="opener-holder">
						<a href="#" class="nav-opener"><span></span></a>
					</div>
					<div class="nav-drop">
						<ul>
							<li><a href="../">Inicio</a></li>
							<li><a href="">Con&oacute;cenos</a></li>
							<li><a href="../daycamp">Day Camp</a></li>
							<li><a href="../inscripciones">Inscripciones</a></li>
							<li><a target="_blank" href="https://www.flickr.com/photos/133842989@N07/albums">Galer&iacute;a</a></li>
							<li><a href="#contacto">Contacto</a></li>
						</ul>
					</div>
				</nav>
			</div>
		</header>
	<section class="conoce">
		<div class="texture-overlay"></div>
		<div class="row hero-content">
			<div class="conoce-btn">
				<a href="#conoce" class="learn-btn animated fadeInUp" style="font-size: 25px;">Con&oacute;cenos<i class="fa fa-arrow-down"></i></a>
			</div>
		</div>
		<!-- <div class="row dino-conoce">
			<div class="col-md-offset-2 col-md-1">
				<img src="../img/new-site/dino.png" />
			</div>
			<div class="col-md-8" style="margin-left:20px">
				<h2 class="animated fadeInDown">Regalando sonrisas a nuestros niños desde 1986.</h2>
			</div>
		</div> -->
	</section>
	<section id="conoce" class="main">
		<div class="container">
			<div class="content">
				<div class="row">
					<div class="col-md-6 vcenter">
						<h3>¿Quienes Somos?</h3>
						<p>Recreaciones 1986 C.A. es una compañ&iacute;a dedicada a la realizaci&oacute;n de Programas Vacacionales y Eventos Recreacionales para niños. Comenzamos nuestra exitosa trayectoria realizando este tipo de actividades en 1986, año en que iniciamos los Campamentos Vacacionales conocidos como Diana and Dino's Camp, los cuales se desarrollan actualmente en el Club Internacional de Guataparo. </p>

						<p>Nuestros campamentos y programas tienen como objetivo la recreaci&oacute;n del niño en un ambiente educativo, resaltando los valores y su formaci&oacute;n individual a trav&eacute;s de la actividad grupal. </p>

						<p>Desarrollamos nuestras actividades con programas corporativos diseñados especialmente para cada empresa, y con nuestros tradicionales campamentos Day Camp en el Club Internacional de Guataparo. </p>
					</div>
					<div class="col-md-5 vcenter">
							<img class="img-responsive img-margin" src="../images/viejitas1.jpg" alt="viejita1">
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 vcenter">
						<p>A lo largo de todos estos años hemos realizado planes vacacionales a empresas, colegios, clubes, organizaciones, fundaciones, compañ&iacute;as de seguros, l&iacute;neas a&eacute;reas, instituciones bancarias y particulares, manejando grupos hasta de 400 niños por d&iacute;a y distintas modalidades, como Day Camp, pernoctas, planes de visitas diarias, paseos, fiestas, diversos tipos de eventos y tours para niños y adultos a Disney World. </p>

						<p>Hemos logrado perdurar en el tiempo gracias al amor por lo que hacemos y a nuestro empeño y dedicaci&oacute;n para lograr un &oacute;ptimo funcionamiento mientras hacemos felices a nuestros niños. Es emocionante ver a campistas del pasado llevar ahora a sus hijos a participar del campamento. </p>
					</div>
					<div class="col-md-5 vcenter">
						<img class="img-responsive img-margin" src="../images/viejitas2.jpg" alt="viejita2">
					</div>
				</div>
				
			</div>
		</div>
	</section>
	<?php include('../customize/section_footer.php') ?>
	<!-- <section id="contacto" class="main">
		<footer id="footer">
			<div class="container">
				<div id="cta">
					<a href="https://instagram.com/dinoscamp" class="btn btn-blue"><i class="fa fa-instagram pr"></i>Instagram</a>
					<p>S&iacute;guenos en Instagram</p>
				</div>
				<div class="footer-holder">
					<div class="row">
						<div class="col-md-3">
							<div class="logo">
								<img src="../images/logo_original.png" alt="LOGO" style="height: 100%;">
							</div>
						</div>
						<div class="col-md-3">
							<h4>Correo</h4>
							<ul>
								<li><a>dianacogorno@gmail.com</a></li>
							</ul>
						</div>
						<div class="col-md-3">
							<h4>Tel&eacute;fonos</h4>
							<ul>
								<li><span class="available">Diana Cogorno de Arias</span></li>
								<li><a>0414-4444294 <br>0414-4366552</a></li>
								<li><a>0241-4155431<br>0241-8223769</a></li>
							</ul>
						</div>
						<div class="col-md-3">
							<h4>Direcci&oacute;n Oficina</h4>
							<ul>
								<li><a>Urb. La Viña, Calle P&aacute;ez #108-170.</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</footer>
	</section> -->
</div>
<script src="../js/jquery-1.11.2.min.js"></script>
<script src="../js/bootstrap.js"></script>
<script src="../js/jquery.main.js"></script>
<script src="../js/classie.js"></script>
<script src="../js/SmoothScrolling.js"></script>
<script>
	    function init() {
	        window.addEventListener('scroll', function(e){
	            var distanceY = window.pageYOffset || document.documentElement.scrollTop,
	                shrinkOn = 500,
	                header = document.querySelector("header");
	            if (distanceY > shrinkOn) {
	                classie.add(header,"smaller");
	                $( ".smaller" ).fadeIn();
	            } else {
	                if (classie.has(header,"smaller")) {
	                    classie.remove(header,"smaller");
	                }
	            }
	        });
	    }
	    window.onload = init();
	</script>
</body>
</html>