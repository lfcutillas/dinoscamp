<?php

//require_once("adodb/adodb.inc.php");
	/*$fechas = '1) Del 07 al 10 de Agosto <br>
									2) Del 14 al 17 de Agosto <br>
									3) Del 21 al 24 de Agosto <br>
									4) Del 28 al 31 de Agosto <br>
									5) Del 04 al 07 de Septiembre';*/
require_once("customize/texts.php");
?>
<!DOCTYPE html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Diana and Dino's Camp</title>
	<meta name="description" content="">
	<meta name="keywords" content="website template, css3, one page, bootstrap, app template, web app, start-up">
	<meta name="author" content="Pixel Buddha and PSD2HTML for Codrops">
	<link rel="shortcut icon" href="favicons/favicon.ico">
	<meta name="msapplication-TileColor" content="#603cba">
	<meta name="msapplication-TileImage" content="favicons/mstile-144x144.png">
	<meta name="msapplication-config" content="favicons/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="fonts/font-awesome-4.3.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="css/all.css">
	<link rel="stylesheet" href="css/slick-theme.css">
	<link rel="stylesheet" href="css/slick.css"/>
</head>
<body>
	<script src="js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="js/jssor/jssor.slider.mini.js"></script>
    <script>
        jQuery(document).ready(function ($) {

            var _CaptionTransitions = [];
            _CaptionTransitions["STOP|8"] = { $Duration: 10000 };
            _CaptionTransitions["L"] = { $Duration: 900, x: 0.6, $Easing: { $Left: $JssorEasing$.$EaseInOutSine }, $Opacity: 2 };
            _CaptionTransitions["R"] = { $Duration: 900, x: -0.6, $Easing: { $Left: $JssorEasing$.$EaseInOutSine }, $Opacity: 2 };
            _CaptionTransitions["T"] = { $Duration: 900, y: 0.6, $Easing: { $Top: $JssorEasing$.$EaseInOutSine }, $Opacity: 2 };
            _CaptionTransitions["B"] = { $Duration: 900, y: -0.6, $Easing: { $Top: $JssorEasing$.$EaseInOutSine }, $Opacity: 2 };
            _CaptionTransitions["ZMF|10"] = { $Duration: 900, $Zoom: 11, $Easing: { $Zoom: $JssorEasing$.$EaseOutQuad, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2 };
            _CaptionTransitions["RTT|10"] = { $Duration: 900, $Zoom: 11, $Rotate: 1, $Easing: { $Zoom: $JssorEasing$.$EaseOutQuad, $Opacity: $JssorEasing$.$EaseLinear, $Rotate: $JssorEasing$.$EaseInExpo }, $Opacity: 2, $Round: { $Rotate: 0.8} };
            _CaptionTransitions["RTT|2"] = { $Duration: 900, $Zoom: 1, $Rotate: 1, $Easing: { $Zoom: $JssorEasing$.$EaseInQuad, $Opacity: $JssorEasing$.$EaseLinear, $Rotate: $JssorEasing$.$EaseInQuad }, $Opacity: 2, $Round: { $Rotate: 0.5} };
            _CaptionTransitions["RTTL|BR"] = { $Duration: 900, x: -0.6, y: -0.6, $Zoom: 11, $Rotate: 1, $Easing: { $Left: $JssorEasing$.$EaseInCubic, $Top: $JssorEasing$.$EaseInCubic, $Zoom: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear, $Rotate: $JssorEasing$.$EaseInCubic }, $Opacity: 2, $Round: { $Rotate: 0.8} };
            _CaptionTransitions["CLIP|LR"] = { $Duration: 900, $Clip: 15, $Easing: { $Clip: $JssorEasing$.$EaseInOutCubic }, $Opacity: 2 };
            _CaptionTransitions["MCLIP|L"] = { $Duration: 900, $Clip: 1, $Move: true, $Easing: { $Clip: $JssorEasing$.$EaseInOutCubic} };
            _CaptionTransitions["MCLIP|R"] = { $Duration: 900, $Clip: 2, $Move: true, $Easing: { $Clip: $JssorEasing$.$EaseInOutCubic} };
            _CaptionTransitions["DDGDANCE|RB"] = { $Duration: 1800, x: -0.3, y: -0.3, $Zoom: 1, $Easing: { $Left: $JssorEasing$.$EaseInJump, $Top: $JssorEasing$.$EaseInJump, $Zoom: $JssorEasing$.$EaseOutQuad }, $Opacity: 2, $During: { $Left: [0, 0.8], $Top: [0, 0.8] }, $Round: { $Left: 0.8, $Top: 2.5} };
             _CaptionTransitions["L|IB"] = { $Duration: 2200, x: 0.6, $Zoom: 8, $Easing: { $Zoom: $JssorEasing$.$EaseInCubic }, $Opacity: 0 };

            var options = {$FillMode: 2,$AutoPlay: true,$AutoPlayInterval: 1000,$PauseOnHover: 1,$ArrowKeyNavigation: true,$SlideEasing: $JssorEasing$.$EaseOutQuint,$SlideDuration: 800,$MinDragOffsetToSlide: 20,$SlideSpacing: 0,$DisplayPieces: 1,$ParkingPosition: 0,$UISearchMode: 1,$PlayOrientation: 1,$DragOrientation: 1,
            	$CaptionSliderOptions: {                            //[Optional] Options which specifies how to animate caption
                    $Class: $JssorCaptionSlider$,                   //[Required] Class to create instance to animate caption
                    $CaptionTransitions: _CaptionTransitions,       //[Required] An array of caption transitions to play caption, see caption transition section at jssor slideshow transition builder
                    $PlayInMode: 1,                                 //[Optional] 0 None (no play), 1 Chain (goes after main slide), 3 Chain Flatten (goes after main slide and flatten all caption animations), default value is 1
                    $PlayOutMode: 3                                 //[Optional] 0 None (no play), 1 Chain (goes before main slide), 3 Chain Flatten (goes before main slide and flatten all caption animations), default value is 1
                },

                $BulletNavigatorOptions: {                          //[Optional] Options to specify and enable navigator or not
                    $Class: $JssorBulletNavigator$,                 //[Required] Class to create navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 1,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
                    $Lanes: 1,                                      //[Optional] Specify lanes to arrange items, default value is 1
                    $SpacingX: 8,                                   //[Optional] Horizontal space between each item in pixel, default value is 0
                    $SpacingY: 8,                                   //[Optional] Vertical space between each item in pixel, default value is 0
                    $Orientation: 1                                 //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
                },

                $ArrowNavigatorOptions: {                           //[Optional] Options to specify and enable arrow navigator or not
                    $Class: $JssorArrowNavigator$,                  //[Requried] Class to create arrow navigator instance
                    $ChanceToShow: 1,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 2,                                 //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
                }
            };

            var jssor_slider1 = new $JssorSlider$("slider1_container", options);
            //responsive code
            function ScaleSlider() {
                var bodyWidth = document.body.clientWidth;
                if (bodyWidth)
                    jssor_slider1.$ScaleWidth(Math.min(bodyWidth, 1920));
                else
                    window.setTimeout(ScaleSlider, 30);
            }
            ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
        });
    </script>
<div id="wrapper">
		<header id="header">
			<div class="container">
				<nav id="nav">
					<div class="opener-holder">
						<a href="#" class="nav-opener"><span></span></a>
					</div>
					<div class="nav-drop">
						<ul>
							<li><a href="conocenos">Con&oacute;cenos</a></li>
							<li><a href="daycamp">Day Camp</a></li>
							<li><a href="inscripciones">Inscripciones</a></li>
							<li><a target="_blank" href="https://www.flickr.com/photos/133842989@N07/albums">Galer&iacute;a</a></li>
							<li><a href="#contacto">Contacto</a></li>
						</ul>
					</div>
				</nav>
			</div>
		</header>
	<section>
		<!-- it works the same with all jquery version from 1.x to 2.x -->
	    <!-- <script type="text/javascript" src="../js/jquery-1.9.1.min.js"></script> -->
	    
	    <!-- Jssor Slider Begin -->
	    <!-- To move inline styles to css file/block, please specify a class name for each element. --> 
	    <div id="slider1_container" style="position: relative; margin: 0 auto;
	        top: 0px; left: 0px; width: 1300px; height: 550px; overflow: hidden;">
	        <!-- Loading Screen -->
	        <div u="loading" style="position: absolute; top: 0px; left: 0px;">
	            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block;
	                top: 0px; left: 0px; width: 100%; height: 100%;">
	            </div>
	            <div style="position: absolute; display: block; background: url(img/loading.gif) no-repeat center center;
	                top: 0px; left: 0px; width: 100%; height: 100%;">
	            </div>
	        </div>
	        <!-- Slides Container -->
	        <div u="slides" style="cursor: default; position: absolute; left: 0px; top: 0px; width: 1300px;
	            height: 550px; overflow: hidden;">
	            <div id="portada" >
	                <img u="image" src="img/1920/fondo_portada.png" />
	                <div class="titulo" style="position: absolute; width: 480px; height: 120px; top: 30px; left: 30px; padding: 5px;
	                    text-align: left; line-height: 60px; text-transform: uppercase; font-size: 50px;
	                        color: #FFFFFF;">
	                    <img src="img/new-site/titulo.png" style="position: absolute; top: 50px; left: 200px;" />
	                </div>
	                <div u="caption" t="L|IB" t2="NO" style="position: absolute; width: 445px; height: 300px; top: 100px; left: 650px;">
	                	<img src="img/new-site/dino.png" style="position: absolute;  top: 160px; left: -10px;" />
	                </div >
	                <div u="caption"  t="DDGDANCE|RB" t2="RTT|10" d="-1600" du="3500" t3="NO"  style="position: absolute; width: 445px; height: 300px; top: 100px; left: 720px;">
	                	<img src="img/new-site/pelota.png" style="position: absolute;  top: 400px; left: -20px;" />
	                </div >
	                <!-- fotoportada1 -->
	                <div u="caption" t="RTT|2" r="-75%" d="0" du="1000" style="position: absolute; width: 445px; height: 300px; top: 100px; left: 750px;">
	                    <img u="caption" t2="RTTL|BR" d2="2000" du2="2000" t3="RTT|2" d3="2000" du3="1500" src="img/new-site/fotoportada1.png" style="position: absolute; width: 445px; height: 300px; top: 4px; left: 0px;" />
	                </div>
	                <!-- fotoportada2 -->
	                <div u="caption" t="RTT|2" r="-75%" d="2000" du="1000" style="position: absolute; width: 445px; height: 300px; top: 100px; left: 750px;">
	                    <img u="caption" t2="RTTL|BR" d2="2000" du2="1500" t3="RTT|2" d3="2000" du3="1500" src="img/new-site/fotoportada2.png" style="position: absolute; width: 445px; height: 300px; top: 4px; left: 0px;" />
	                </div>
	                <!-- fotoportada3 -->
	                <div u="caption" t="RTT|2" r="-75%" d="2000" du="1000" style="position: absolute; width: 445px; height: 300px; top: 100px; left: 750px;">
	                    <img u="caption" t2="RTTL|BR" d2="2000" du2="1500" t3="RTT|2" d3="2000" du3="1500" src="img/new-site/fotoportada3.png" style="position: absolute; width: 445px; height: 300px; top: 4px; left: 0px;" />
	                </div>
	                <!-- fotoportada4 -->
	                <div u="caption" t="RTT|2" r="-75%" d="2000" du="1000" style="position: absolute; width: 445px; height: 300px; top: 100px; left: 750px;">
	                    <img u="caption" t2="RTTL|BR" d2="2000" du2="1500" t3="RTT|2" d3="2000" du3="1500" src="img/new-site/fotoportada4.png" style="position: absolute; width: 445px; height: 300px; top: 4px; left: 0px;" />
	                </div>
	                
	            </div>
	            <div>
	                <img u="image" src="img/1920/fondo_temporada.png" />
	                <div u="caption" style="position: absolute;">
	                    <img u="caption" t="STOP|8" r="-75%" d="2000" du="1000" src="img/new-site/titulo_temporada.png" style="position: absolute; left: 600px; top: 60px;" />
	                </div>
	                <div style="position: absolute;
								width: 480px;
								height: 120px;
								top: 130px;
								left: 100px;
								padding: 5px;
								text-align: left;
								line-height: 60px;
								font-size: 25px;
								color: #FFF;">
								<?php echo $fechas; ?>
	                </div>
	                <span style="position: absolute;bottom: 3px;color: #fff;left: 250px; font-size: 14px;">Debido a la situaci&oacute;n pa&iacute;s las fechas y horarios pudieran modificarse pensando siempre en la seguridad de los campistas.</span><br>
	            </div>
	            <div>
	                <img u="image" src="img/1920/fondo_contacto.png" />
	                <div u="caption" t="STOP|8" r="-75%" d="2000" du="1000" style="position: absolute; width: 480px; height: 120px; top: 100px; left: 70px; padding: 5px;
	                    text-align: left; line-height: 60px; text-transform: uppercase; font-size: 50px;
	                        color: #FFFFFF;">
	                        Contacto
	                </div>
	                <div style="position: absolute; width: 450px; height: 120px; top: 100px; left: 450px; padding: 5px;
	                    text-align: left; line-height: 60px; text-transform: uppercase; font-size: 50px;
	                        color: #FFFFFF;">
	                        <img u="image" src="images/new.png" style="width: 350px;"/>
	                </div>
	                <div style="position: absolute; width: 480px; height: 120px; top: 340px; left: 0px; padding: 0px;
	                    text-align: left; line-height: 36px; font-size: 30px;
	                        color: #FFFFFF;">
	                        <img u="image" src="images/planescorporativos.png" style="width: 360px;"/>
	                </div>
	                <div style="position: absolute;
								width: 480px;
								height: 100px;
								top: 100px;
								left: 750px;
								padding: 5px;
								text-align: right;
								line-height: 30px;
								font-size: 20px;
								color: #FFF;">
								Correo: dianacogorno@gmail.com<br><br>
								Instagram: @dinoscamp <br><br>
								
	                        	Tel&eacute;fonos: 0424-4444294<br>
											0414-4366552<br>
											<br>
								Direcci&oacute;n:<br> Club Internacional de Guataparo.<br> Valencia, Venezuela.<br>

	                </div>
	            </div>
	        </div>
	                
	        <!--#region Bullet Navigator Skin Begin -->
	        <!-- Help: http://www.jssor.com/development/slider-with-bullet-navigator-jquery.html -->
	        <style>
	            /* jssor slider bullet navigator skin 21 css */
	            /*
	            .jssorb21 div           (normal)
	            .jssorb21 div:hover     (normal mouseover)
	            .jssorb21 .av           (active)
	            .jssorb21 .av:hover     (active mouseover)
	            .jssorb21 .dn           (mousedown)
	            */
	            .jssorb21 {
	                position: absolute;
	            }
	            .jssorb21 div, .jssorb21 div:hover, .jssorb21 .av {
	                position: absolute;
	                /* size of bullet elment */
	                width: 19px;
	                height: 19px;
	                text-align: center;
	                line-height: 19px;
	                color: white;
	                font-size: 12px;
	                background: url(img/b21.png) no-repeat;
	                overflow: hidden;
	                cursor: pointer;
	            }
	            .jssorb21 div { background-position: -5px -5px; }
	            .jssorb21 div:hover, .jssorb21 .av:hover { background-position: -35px -5px; }
	            .jssorb21 .av { background-position: -65px -5px; }
	            .jssorb21 .dn, .jssorb21 .dn:hover { background-position: -95px -5px; }
	        </style>
	        <!-- bullet navigator container -->
	        <div u="navigator" class="jssorb21" style="bottom: 26px; right: 6px;">
	            <!-- bullet navigator item prototype -->
	            <div u="prototype"></div>
	        </div>
	        <!--#endregion Bullet Navigator Skin End -->
	        
	        <!--#region Arrow Navigator Skin Begin -->
	        <!-- Help: http://www.jssor.com/development/slider-with-arrow-navigator-jquery.html -->
	        <style>
	            /* jssor slider arrow navigator skin 21 css */
	            /*
	            .jssora21l                  (normal)
	            .jssora21r                  (normal)
	            .jssora21l:hover            (normal mouseover)
	            .jssora21r:hover            (normal mouseover)
	            .jssora21l.jssora21ldn      (mousedown)
	            .jssora21r.jssora21rdn      (mousedown)
	            */
	            .jssora21l, .jssora21r {
	                display: block;
	                position: absolute;
	                /* size of arrow element */
	                width: 55px;
	                height: 55px;
	                cursor: pointer;
	                background: url(img/a21.png) center center no-repeat;
	                overflow: hidden;
	            }
	            .jssora21l { background-position: -3px -33px; }
	            .jssora21r { background-position: -63px -33px; }
	            .jssora21l:hover { background-position: -123px -33px; }
	            .jssora21r:hover { background-position: -183px -33px; }
	            .jssora21l.jssora21ldn { background-position: -243px -33px; }
	            .jssora21r.jssora21rdn { background-position: -303px -33px; }
	        </style>
	        <!-- Arrow Left -->
	        <span u="arrowleft" class="jssora21l" style="top: 123px; left: 8px;">
	        </span>
	        <!-- Arrow Right -->
	        <span u="arrowright" class="jssora21r" style="top: 123px; right: 8px;">
	        </span>
	        <!--#endregion Arrow Navigator Skin End -->
	        <a style="display: none" href="http://www.jssor.com">Thumbnail Slider</a>
	    </div>
	    <!-- Jssor Slider End -->
	</section>
	<section class="noslider">
		<div class="container">
			<div class="row">
				<div class="text-box col-md-offset-1 col-md-10">
					<img class="rtitulo" src="img/new-site/titulo.png"/>
				</div>
			</div>
		</div>
	</section>
	<section class="noslider" style="background: rgb(26, 152, 255) none repeat scroll 0% 0%;">
		<div class="container">
			<div class="row">
				<div class="text-box col-md-offset-1 col-md-10">
					<img class="rtitulo-temporada" src="img/new-site/titulo_temporada.png" />
				</div>
			</div>
			<div class="row">
				<div class="text-center" style="color: #FFF;">
					<span>Debido a la situaci&oacute;n pa&iacute;s las fechas y horarios pudieran modificarse pensando siempre en la seguridad de los campistas.</span><br>
					<?php echo $fechas; ?>
                </div>
			</div>
		</div>
		<img u="image" src="img/1920/fondo_temporada.png" style="width:100%;"/>
	</section>
	<section class="area" style="padding-bottom: 90px;">
		<div class="container">
			<div class="row">
				<div class="text-box col-md-offset-1 col-md-10">
					<h2>Campamento Vacacional</h2>
					<p>Day Camp</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<ul class="visual-list">
						<li>
							<div class="img-holder">
								<img src="images/experiencia.png" height="100" alt="">
							</div>
							<div class="text-holder">
								<h3>Experiencia</h3>
								<p>Dino's Camp cumple 33 a&ntilde;os de fundado y 29 en el Club Internacional. Desde 1986 regalando sonrisas a nuestros ni&ntilde;os.</p>
							</div>
						</li>
						<li>
							<div class="img-holder">
								<img src="images/edades.png" width="80" alt="">
							</div>
							<div class="text-holder">
								<h3>Edades</h3>
								<p>Para ni&ntilde;os entre 3 y 13 a&ntilde;os</p>
							</div>
						</li>
						<li>
							<div class="img-holder">
								<img src="images/transporte.png" width="90" alt="">
							</div>
							<div class="text-holder">
								<h3>Transporte</h3>
								<p>Ofrecemos servicio de transporte opcional. </p>
							</div>
						</li>
						<li>
							<div class="img-holder">
								<img src="images/comida.png" height="84" alt="">
							</div>
							<div class="text-holder">
								<h3>Almuerzos y Meriendas</h3>
								<p>Nuestro plan incluye almuerzos y meriendas. Sugerimos revise el <a href="daycamp">Men&uacute;</a> </p>
							</div>
						</li>
					</ul>
				</div>
				<div class="col-md-8">
					<div class="slide-holder">
						<!-- <h2 class="hidden-xs hidden-sm text-primary"></h2> -->
						<div class="img-slide scroll-trigger"><img src="images/actividades.png" alt=""></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="text-box col-md-5">
					<a class="btn btn-primary rounded" href="daycamp">Leer M&aacute;s</a>
				</div>
			</div>
		</div>
	</section>
	<?php include('customize/section_precios.php') ?>
	<!-- <section class="visual-container">
		<div class="visual-area">
			<div class="container">
				<div class="text-box">
					<h2 style="color:#626262;">Precios</h2>
					<p style="margin: 0px 0px 70px; color: rgb(102, 102, 102); font-size: 1.6rem;">V&aacute;lidos hasta el 10 de agosto.</p>
				</div>
				
				<div class="pricing-tables">
		            <div class="plan">
		                <div class="head">
		                    <h3>Inscripci&oacute;n</h3> </div>
		                <div class="price">
		                    <span class="price-main"><span class="symbol">BsF</span>45.000</span>
		                    <span class="price-additional">pago &uacute;nico</span>
		                </div>
		                    <ul class="item-list">
		                       <li>Un solo pago, sin importar el numero de semanas en las que participe</li>
		                       <li>Incluye el seguro y una franela del Campamento</li>
		                       <li>Franela Adicional BsF. 30.000</li>
		                    </ul>
		            </div>
		            <div class="plan recommended">
		                <div class="head">
		                    <h3>Socios</h3> </div>
		                <div class="price">
		                    <span class="price-main"><span class="symbol">BsF</span>350.000</span>
		                    <span class="price-additional">por semana</span>
		                </div>
		                    <ul class="item-list">
		                        <li>Incluye 8 horas diarias de atenci&oacute;n, material de apoyo para la realizaci&oacute;n de actividades, almuerzos, meriendas, distintivo y recuerdo del Campamento.</li>
		                    
		                    </ul>
		            </div>
		            <div class="plan">
		                <div class="head">
		                    <h3>No Socios</h3> </div>
		                <div class="price">
		                    <span class="price-main"><span class="symbol">BsF</span>430.000</span>
		                    <span class="price-additional">por semana</span>
		                </div>
		                <ul class="item-list">
		                    <li>Incluye 8 horas diarias de atenci&oacute;n, material de apoyo para la realizaci&oacute;n de actividades, almuerzos, meriendas, distintivo y recuerdo del Campamento.</li>
		                </ul>
		            </div>
				</div>
				<p class="silent">Estos precios no incluyen IVA</p>
			</div>
		</div>
	</section> -->
	<!-- <section class="area">
		<div class="container">
			<div class="subscribe">
				<p>Viste suficiente? Comienza el proceso de Inscripci&oacute;n!</p>
				<a class="btn btn-primary rounded" href="inscripciones">Ir a Inscripciones</a>
			</div>
		</div>
	</section> -->
	<section class="main"  style="background: rgb(71, 150, 218) none repeat scroll 0% 0%;">
		<div class="container">
			<div class="testimonios">
				<div class="text-box">
					<h2>Testimonios</h2>
				</div>
				<div class="slider responsive">
					<div>
						<div>
							<iframe width="100%" height="315" src="https://www.youtube.com/embed/DrV-7vYZGT8" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
					<div>
						<div>
							<iframe width="100%" height="315" src="https://www.youtube.com/embed/ZQvksqouAjY" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
					<div>
						<div>
							<iframe width="100%" height="315" src="https://www.youtube.com/embed/YXnXqIFe5pU" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
					<div>
						<div>
							<iframe width="100%" height="315" src="https://www.youtube.com/embed/zuTEw6ci38M" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="main">
		<div class="container">
				<div id="cta">
					<a href="https://instagram.com/dinoscamp" class="btn btn-blue"><i class="fa fa-instagram pr"></i>Instagram</a>
					<p style="color: rgb(153, 153, 153);">S&iacute;guenos en Instagram</p>
				</div>
		</div>
		<div class="container">
			<div id="instafeed" class="instafeed"></div>
		</div>
	</section>
	<!-- <section id="contacto" class="main">
		<footer id="footer">
			<div class="container">
				
				<div class="footer-holder">
					<div class="row">
						<div class="col-md-3">
							<div class="logo">
								<img src="images/logo_original.png" alt="FORKIO" style="height: 100%;">
							</div>
						</div>
						<div class="col-md-3">
							<h4>Correo</h4>
							<ul>
								<li><a>dianacogorno@gmail.com</a></li>
							</ul>
						</div>
						<div class="col-md-3">
							<h4>Tel&eacute;fonos</h4>
							<ul>
								<li><span class="available">Diana Cogorno de Arias</span></li>
								<li><a>0414-4444294 <br>0414-4366552</a></li>
								<li><a>0241-4155431<br>0241-8223769</a></li>
							</ul>
						</div>
						<div class="col-md-3">
							<h4>Direcci&oacute;n Oficina</h4>
							<ul>
								<li><a>Urb. La Vi&ntilde;a, Calle P&aacute;ez #108-170.</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</footer>
	</section> -->
	<?php include('customize/main_footer.php') ?>
</div>

	<script src="js/bootstrap.js"></script>
	<script src="js/jquery.main.js"></script>
	<script src="js/classie.js"></script>
	<script type="text/javascript" src="js/instafeed.min.js"></script>
	<script type="text/javascript" src="js/slick.min.js"></script>
					
	<script type="text/javascript">
	    var userFeed = new Instafeed({
	        get: 'user',
	        userId: 329229869,
	        accessToken: '329229869.467ede5.3dc0a84d955248b7a39471152b5727b3',
		    template: '<div><div class="image"><a href="{{link}}"><img src="{{image}}" /></a></div></div>',
		    after: function () {
		        $('.instafeed').slick({ dots: true,
					infinite: false,
					speed: 300,
					slidesToShow: 4,
					slidesToScroll: 4,
					responsive: [
					    {
					      breakpoint: 1024,
					      settings: {
					        slidesToShow: 3,
					        slidesToScroll: 3,
					        infinite: true,
					        dots: true
					      }
					    },
					    {
					      breakpoint: 600,
					      settings: {
					        slidesToShow: 2,
					        slidesToScroll: 2
					      }
					    },
					    {
					      breakpoint: 480,
					      settings: {
					        slidesToShow: 1,
					        slidesToScroll: 1
					      }
					    }
					    // You can unslick at a given breakpoint now by adding:
					    // settings: "unslick"
					    // instead of a settings object
					]
				});
		    }
	    });
	    userFeed.run();
	</script>
	<script>
	    function init() {
	        window.addEventListener('scroll', function(e){
	            var distanceY = window.pageYOffset || document.documentElement.scrollTop,
	                shrinkOn = 100,
	                header = document.querySelector("header");
	            if (distanceY > shrinkOn) {
	                classie.add(header,"smaller");
	                $( ".smaller" ).fadeIn();
	            } else {
	                if (classie.has(header,"smaller")) {
	                    classie.remove(header,"smaller");
	                }
	            }
	        });
	    }
	    window.onload = init();
	</script>
	<script type="text/javascript">
	$(document).ready(function() {

    $('.responsive').slick({
        // dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                // centerMode: true,

            }

        }, {
            breakpoint: 800,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                infinite: true,

            }


        }, {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                dots: true,
                infinite: true,
                
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                infinite: true,
                autoplay: true,
                autoplaySpeed: 2000,
            }
        }]
    });


});</script>
    <script src="js/SmoothScrolling.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-99030485-2', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>