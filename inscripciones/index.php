<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Diana and Dino's Camp</title>
	<meta name="description" content="">
	<meta name="keywords" content="website template, css3, one page, bootstrap, app template, web app, start-up">
	<meta name="author" content="Pixel Buddha and PSD2HTML for Codrops">
	<link rel="shortcut icon" href="favicons/favicon.ico">
	<meta name="msapplication-TileColor" content="#603cba">
	<meta name="msapplication-TileImage" content="../favicons/mstile-144x144.png">
	<meta name="msapplication-config" content="../favicons/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	<link rel="stylesheet" href="../css/bootstrap.css">
	<link rel="stylesheet" href="../fonts/font-awesome-4.3.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="../css/all.css">

</head>
<body>
<div id="wrapper">
		<header id="header">
			<div class="container">
				<nav id="nav">
					<div class="opener-holder">
						<a href="#" class="nav-opener"><span></span></a>
					</div>
					<div class="nav-drop">
						<ul>
							<li><a href="../">Inicio</a></li>
							<li><a href="../conocenos">Con&oacute;cenos</a></li>
							<li><a href="../daycamp">Day Camp</a></li>
							<li><a href="">Inscripciones</a></li>
							<li><a target="_blank" href="https://www.flickr.com/photos/133842989@N07/albums">Galer&iacute;a</a></li>
							<li><a href="#contacto">Contacto</a></li>
						</ul>
					</div>
				</nav>
			</div>
		</header>
	<section class="main">
		<div class="container">
			<div class="content">
				<div class="row">
					<h2>Inscripciones</h2>
					<div class="col-md-8">
						<h4>Procedimiento para Inscripci&oacute;n:</h4>
						<table class="table table-hover">
							<tr class="colorealo">
								<td class="numbers">1</td>
							  	<td class="text"><p>Verificar telefónicamente (0424-4444294) la disponibilidad de cupo y los precios actualizados el mismo día en que vaya a realizar el pago.</p></td>	
							</tr>
							<tr class="colorealogreen">
								<td class="colorealogreen numbers">2</td>
							  	<td class="text"><p>Llenar  <a class="btn btn-primary rounded" href="https://docs.google.com/forms/d/1IN9pVb6fTiL_xFWCaEFLEPZZXn9c5hFULyX9tF7SFg0/viewform" target="_blank">Planilla Online</a> (Si se inscribe con menos de 3 dias de anticipación deberá llenar tambien la planilla en físico en la oficina).</p></td>	
							</tr>
							<tr class="colorealo">
								<td class="numbers">3</td>
							  	<td class="text"><p>El representante legal deberá leer y firmar el contrato de aceptación de las políticas del campamento.<a style="float: right;margin: 9px;" class="btn btn-primary rounded" href="ContratoDeAceptacionDePoliticasDianaAndDinosCamp.pdf" target="_blank">Descargar Contrato</a></p></td>	
							</tr>
							<tr class="colorealogreen">
								<td class="numbers">4</td>
							  	<td class="text">
							  		<p>Formalizar el pago correspondiente.<a class="btn btn-primary rounded" href="#pay">Ver Formas de Pago</a> Recuerde que los precios serán ajustados con regularidad.</p>
							  	</td>	
							</tr>
							<tr class="colorealo">
								<td class="numbers">5</td>
							  	<td class="text"><p>Dirigirse a las oficinas, llevar el contrato firmado y el pago o comprobante de transferencia o deposito.</p></td>	
							</tr>
						</table>
						<!-- <p>1) Verificar disponibilidad cupos: llamar a (0424-4444294).<br>2) Llenar  <a class="btn btn-primary rounded" href="https://docs.google.com/forms/d/1IN9pVb6fTiL_xFWCaEFLEPZZXn9c5hFULyX9tF7SFg0/viewform" target="_blank">Planilla Online</a> (Si se inscribe con menos de 3 dias de anticipación deberá llenar tambien la planilla en físico en la oficina).</p>
						<p>3) El representante legal deberá leer y firmar el contrato de aceptación de las políticas del campamento.<a class="btn btn-primary rounded" href="planillas_inscripcion_2015.pdf" target="_blank">Descargar Contrato</a>
						<br>4) Formalizar el pago.</p> -->
						<p class="bg-warning-pasos"> * No se considerará inscrito el campista que no haya cumplido TODOS los pasos anteriores.</p>
					</div>
					<div class="col-md-4">
						<h4>Direcci&oacute;n 1: <br>Oficina La Vi&ntilde;a</h4>
						<p>En la urbanizaci&oacute;n La Vi&ntilde;a, Calle P&aacute;ez #108-170. Tel&eacute;fonos: 0412-7704366 y 0241-8214643.<br>
							De 9:00 am a 4:00 pm<br>
							Los días viernes 12, 19 y 26 de Julio.<br>
							De lunes a jueves del 22 al 25 de Julio.<br>   
							Los días viernes 2, 9, 16 y 24 de Agosto.<br>
						</p>
						<h4>Direcci&oacute;n 2: <br> Oficina Club Internacional de Guataparo</h4>
						<p>En el Club Internacional de Guataparo. <br>De Lunes a Jueves de 9:00 a.m. a 5:00 p.m. <br>Abiertas a partir del 29 de Julio.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="pay" class="visual-container pay" style="background: #FFA726">
		<div class="visual-area">
			<div class="container">
				<h2>Formas de pago</h2>
				<ul class="testimonials">
					
					<li>
						<div class="img-holder"><a ><img src="../images/deposito.png" height="190" width="190" alt="Deposito Bancario"></a></div>
						<h4>Transferencia v&iacute;a Zelle</h4>
					</li>
					<li>
						<div class="img-holder"><a ><img src="../images/transferencia.png" height="190" width="190" alt="Transferencia Bancaria"></a></div>
						<h4>Transferencia o Pago Movil</h4>
						<!-- <p><em>Preferiblemente del mismo banco (Mercantil, BOD, Venezuela o Banesco).<br>
						Si no es del mismo banco transfiera al Mercantil (solo hasta tres dias antes de la semana que este cancelando)
						<br> -->
<!-- Se aceptaran transferencias hasta 3 d&iacute;as antes del inicio de la semana que est&eacute; cancelando. Es indispensable presentar en f&iacute;sico la constancia de transferencia a la administraci&oacute;n. --><!-- </em></p> -->
					</li>
					<!-- <li>
						<div class="img-holder"><a ><img src="../images/cheque.png" height="190" width="190" alt="Cheque"></a></div>
						<h4>Cheque</h4>
						<p><em>Solo se aceptan pagos con cheques conformables con una anticipaci&oacute;n de 10 o m&aacute;s d&iacute;as.</em></p>
					</li> -->
					<li>
						<div class="img-holder"><a ><img src="../images/efectivo.png" height="190" width="190" alt="Efectivo"></a></div>
						<h4>Efectivo</h4>
						<!-- <p><em>Montos que no excedan al valor de 1 semana de campamento.</em></p> -->
					</li>
				</ul>
				<p style="font-size: 16px;">De contado al momento de la inscripci&oacute;n.</p>
				<p style="font-size: 16px;">Si desea transferir o depositar llame al 04144366552 &oacute; al 04244444294, confirme la disponibilidad de cupo y solicite el n&uacute;mero de cuenta.</p>
				<p style="font-size: 16px;">
					Es indispensable enviar los comprobantes de pago por whatsapp a los números telefónicos del campamento.
				</p>
			</div>
			<!-- <img src="../images/img-decor-02.jpg" height="764" width="1380" alt="" class="bg-stretch"> -->
		</div>
	</section>
	
	<?php include('../customize/section_precios.php') ?>
	<?php include('../customize/section_footer.php') ?>
	<!-- <section id="contacto" class="main">
		<footer id="footer">
			<div class="container">
				<div id="cta">
					<a href="https://instagram.com/dinoscamp" class="btn btn-blue"><i class="fa fa-instagram"></i>Instagram</a>
					<p>S&iacute;guenos en Instagram</p>
				</div>
				<div class="footer-holder">
					<div class="row">
						<div class="col-md-3">
							<div class="logo">
								<img src="../images/logo_original.png" alt="FORKIO" style="height: 100%;">
							</div>
						</div>
						<div class="col-md-3">
							<h4>Correo</h4>
							<ul>
								<li><a>dianacogorno@gmail.com</a></li>
							</ul>
						</div>
						<div class="col-md-3">
							<h4>Tel&eacute;fonos</h4>
							<ul>
								<li><span class="available">Diana Cogorno de Arias</span></li>
								<li><a>0414-4444294 <br>0414-4366552</a></li>
								<li><a>0241-4155431<br>0241-8223769</a></li>
							</ul>
						</div>
						<div class="col-md-3">
							<h4>Direcci&oacute;n Oficina</h4>
							<ul>
								<li><a>Urb. La Vi&ntilde;a, Calle P&aacute;ez #108-170.</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</footer>
	</section> -->
</div>
<script src="../js/jquery-1.11.2.min.js"></script>
<script src="../js/bootstrap.js"></script>
<script src="../js/jquery.main.js"></script>
<script src="../js/classie.js"></script>
<script src="../js/SmoothScrolling.js"></script>
<script>
	    function init() {
	        window.addEventListener('scroll', function(e){
	            var distanceY = window.pageYOffset || document.documentElement.scrollTop,
	                shrinkOn = 1300,
	                header = document.querySelector("header");
	            if (distanceY > shrinkOn) {
	                classie.add(header,"smaller");
	                $( ".smaller" ).fadeIn();
	            } else {
	                if (classie.has(header,"smaller")) {
	                    classie.remove(header,"smaller");
	                }
	            }
	        });
	    }
	    window.onload = init();
	</script>
</body>
</html>