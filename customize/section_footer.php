<section id="contacto" class="main">
	<footer id="footer">
		<div class="container">
			<div id="cta">
				<a href="https://instagram.com/dinoscamp" class="btn btn-blue"><i class="fa fa-instagram"></i>Instagram</a>
				<p>S&iacute;guenos en Instagram</p>
			</div>
			<div class="footer-holder">
				<div class="row">
					<div class="col-md-3">
						<div class="logo">
							<img src="../images/logo_original.png" alt="LOGO" style="height: 100%;">
						</div>
					</div>
					<div class="col-md-3">
						<h4>Correo</h4>
						<ul>
							<li><a>dianacogorno@gmail.com</a></li>
						</ul>
					</div>
					<div class="col-md-3">
						<h4>Tel&eacute;fonos</h4>
						<ul>
							<li><span class="available">Diana Cogorno de Arias</span></li>
							<li><a>0424-4444294 <br>0414-4366552</a></li>
						</ul>
					</div>
					<div class="col-md-3">
						<h4>Direcci&oacute;n Oficina</h4>
						<ul>
							<li><a>Urb. La Vi&ntilde;a, Calle P&aacute;ez #108-170.</a></li>
							<li><a>0412-7704366 <br>0241-8214643</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</footer>
</section>