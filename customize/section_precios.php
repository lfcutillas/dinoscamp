<section class="visual-container">
		<div class="visual-area">
			<div class="container">
				<h2 style="margin: 0px 0px 70px; color:#626262;">Precios</h2>
				<p style="margin: 0px 0px 70px; color: rgb(102, 102, 102); font-size: 1.6rem;">Consultar precios telef&oacute;nicamente o en la planilla de inscripci&oacute;n.<br>Los precios se ir&aacute;n ajustando con bastante regularidad por lo que debe consultarlos justo al momento antes de hacer el pago.</p>
				<div class="pricing-tables">
		            <div class="plan">
		                <div class="head">
		                    <h3>Inscripci&oacute;n</h3> </div> Cont&aacute;ctanos telef&oacute;nicamente
		                <!-- <div class="price">
		                    <span class="price-main"><span class="symbol">BsF</span>45.000</span>
		                    <span class="price-additional">pago &uacute;nico</span>
		                </div>
		                    <ul class="item-list">
		                       <li>Un solo pago, sin importar el numero de semanas en las que participe</li>
		                       <li>Incluye el seguro y una franela del Campamento</li>
		                       <li>Franela Adicional BsF. 30.000</li>
		                    </ul> -->
		            </div>
		            <div class="plan recommended">
		                <div class="head">
		                    <h3>Socios</h3> </div> Cont&aacute;ctanos telef&oacute;nicamente
		                <!-- <div class="price">
		                    <span class="price-main"><span class="symbol">BsF</span>350.000</span>
		                    <span class="price-additional">por semana</span>
		                </div>
		                    <ul class="item-list">
		                        <li>Incluye 8 horas diarias de atenci&oacute;n, material de apoyo para la realizaci&oacute;n de actividades, almuerzos, meriendas, distintivo y recuerdo del Campamento.</li>
		                    
		                    </ul> -->
		            </div>
		            <div class="plan">
		                <div class="head">
		                    <h3>No Socios</h3> </div> Cont&aacute;ctanos telef&oacute;nicamente
		                <!-- <div class="price">
		                    <span class="price-main"><span class="symbol">BsF</span>430.000</span>
		                    <span class="price-additional">por semana</span>
		                </div>
		                <ul class="item-list">
		                    <li>Incluye 8 horas diarias de atenci&oacute;n, material de apoyo para la realizaci&oacute;n de actividades, almuerzos, meriendas, distintivo y recuerdo del Campamento.</li>
		                </ul> -->
		            </div>
				</div>
				<!-- <p class="silent">Estos precios no incluyen IVA</p> -->
			</div>
		</div>
		
	</section>