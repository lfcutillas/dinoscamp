<?php
require_once("../customize/texts.php");
?>
<!DOCTYPE html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Diana and Dino's Camp</title>
	<meta name="description" content="">
	<meta name="keywords" content="website template, css3, one page, bootstrap, app template, web app, start-up">
	<meta name="author" content="Pixel Buddha and PSD2HTML for Codrops">
	<link rel="shortcut icon" href="favicons/favicon.ico">
	<meta name="msapplication-TileColor" content="#603cba">
	<meta name="msapplication-TileImage" content="../favicons/mstile-144x144.png">
	<meta name="msapplication-config" content="../favicons/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	<link rel="stylesheet" href="../css/bootstrap.css">
	<link rel="stylesheet" href="../fonts/font-awesome-4.3.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="../css/all.css">
	<link rel="stylesheet" href="../css/set1.css">
	<link href='http://fonts.googleapis.com/css?family=Raleway:400,800,300' rel='stylesheet' type='text/css'>
<!-- 	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700|Source+Sans+Pro:400,700,400italic,700italic' rel='stylesheet' type='text/css'> -->
</head>
<body>
	<div id="wrapper">
		<header id="header" class="smaller">
			<div class="container">
				<nav id="nav">
					<div class="opener-holder">
						<a href="#" class="nav-opener"><span></span></a>
					</div>
					<div class="nav-drop">
						<ul>
							<li><a href="../">Inicio</a></li>
							<li><a href="../conocenos">Con&oacute;cenos</a></li>
							<li><a href="">Day Camp</a></li>
							<li><a href="../inscripciones">Inscripciones</a></li>
							<li><a target="_blank" href="https://www.flickr.com/photos/133842989@N07/albums">Galer&iacute;a</a></li>
							<li><a href="#contacto">Contacto</a></li>
						</ul>
					</div>
				</nav>
			</div>
		</header>
		<section class="daycamp-container">
			<div class="container">
				<div class="row">
					<h3>Day Camp</h3>
					<div class="col-md-6">
						<div class="grid">
							<figure class="effect-milo">
								<img src="../images/daycamp/fechas.jpg" alt="img11"/>
								<figcaption>
									<h2><span>Fechas</span></h2>
									<p>Semanas de camping 2015.</p>
									<a >View more</a>
								</figcaption>
							</figure>
						</div>

						<p><?php echo $fechas ?></p>
						<p>Debido a la situaci&oacute;n pa&iacute;s las fechas y horarios pudieran modificarse pensando siempre en la seguridad de los campistas.</p>
					</div>
					<div class="col-md-6">
						<div class="grid">
							<figure class="effect-milo">
								<img src="../images/daycamp/lugar.jpg" alt="img11"/>
								<figcaption>
									<h2>Lugar &amp; <span>Horario</span></h2>
									<p>Las m&aacute;s c&oacute;modas y seguras instalaciones.</p>
									<a >View more</a>
								</figcaption>
							</figure>
						</div>
						<p>Club Internacional de Guataparo<br>
						De lunes a jueves de 9:00 a.m. a 5:00 p.m.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="grid">
							<figure class="effect-milo">
								<img src="../images/daycamp/actividades.jpg" alt="img11"/>
								<figcaption>
									<h2><span>Actividades</span></h2>
									<p>Ofrecemos m&aacute;s de 20 actividades.</p>
									<a>View more</a>
								</figcaption>
							</figure>
						</div>
						<p>Piscinadas, f&uacute;tbol, b&eacute;isbol, kickingball, basket, tennis, bowling, teatro, bailes, manualidades, caballos, gymkanas, kayaks, spa, cantos, actividades recreativas-educativas, actividades tem&aacute;ticas y especiales.</p>
					</div>
					<div class="col-md-6">
						<div class="grid">
							<figure class="effect-milo">
								<img src="../images/daycamp/uniforme.jpg" alt="img11"/>
								<figcaption>
									<h2><span>Uniforme</span></h2>
									<p>Bonitos recuerdos del camping.</p>
									<a >View more</a>
								</figcaption>
							</figure>
						</div>
						<p> El uso de la franela del campamento es obligatorio. Con el pago de la inscripci&oacute;n se le har&aacute; entrega de una franela, si desea podr&aacute; comprar franelas adicionales.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="grid">
							<figure class="effect-milo">
								<img src="../images/daycamp/comida.jpg" alt="img11"/>
								<figcaption>
									<h2><span>Comidas</span></h2>
									<p>Nuestro plan incluye almuerzos y deliciosas meriendas.</p>
									<a>View more</a>
								</figcaption>
							</figure>
						</div>
						<p>Sugerimos revise el men&uacute; y notifique en la planilla de inscripci&oacute;n si su representado no puede ingerir alguno de estos alimentos. Si su representado tiene una dieta especial deber&aacute; traer lonchera. </p>
					</div>
					<div class="col-md-6">
						<div class="grid">
							<figure class="effect-milo">
								<img src="../images/daycamp/menu.jpg" alt="img11"/>
								<figcaption>
									<h2><span>Men&uacute;</span></h2>
									<p></p>
									<a>View more</a>
								</figcaption>
							</figure>
						</div>
						<p> ALMUERZOS:<br>
						Lunes: Plumitas Bolognesa &oacute; al burro con queso (parmesano o blanco)<br>
						Martes: Pollo a la canasta &oacute; Arroz con pollo.<br>
						Mi&eacute;rcoles: Pizza &oacute; Cachapa con queso de mano o llanero.<br>
						Jueves: Hamburguesa &oacute; pasticho.<br>
						BEBIDAS: <br>
						Limonada, papel&oacute;n con lim&oacute;n, jugos, agua, malta y refrescos.<br>
						MERIENDAS: <br>
						Teque&ntilde;ones, pastelitos, donas, torta, perros calientes, galletas y helados.<br>
						El orden del men&uacute; y alimentos est&aacute; sujeto a cambios.</p>
						
					</div>
					<div class="col-md-6">
						<div class="grid">
							<figure class="effect-milo">
								<img src="../images/daycamp/transporte.jpg" alt="img11"/>
								<figcaption>
									<h2><span>Transporte</span></h2>
									<p></p>
									<a>View more</a>
								</figcaption>
							</figure>
						</div>
						<p>Ofrecemos este servicio opcional para quienes lo requieran, (No aplica a ciertas zonas de la ciudad). El transporte se le cancelar&aacute; directamente a la persona que lo realice.<br>
						 Contacte para el transporte a: <br>
						<!---  Mar&iacute;a Dolores Cogorno 0414 479 3777 (La Vi&ntilde;a, La Alegr&iacute;a, La Carabobo, El Vi&ntilde;edo, Prebo, El Parral, Valle de Camoruco, Los Mangos, El Bosque, Guataparo).<br> -->
						- Mariolga Arias 0414 4377514<!-- (Las Quintas, La Granja, El Rinc&oacute;n, Ma&ntilde;ongo, Guaparo, El Recreo, El Trigal, La Trigale&ntilde;a,  Las Chimeneas, Lomas del Este,  Kerdel, Las Acacias, San Jos&eacute;, Los Colorados, Los N&iacute;speros, Santa Cecilia) -->. </p>
						
					</div>
					<div class="col-md-6">
						<div class="grid">
							<figure class="effect-milo">
								<img src="../images/daycamp/personal.jpg" alt="img11"/>
								<figcaption>
									<h2><span>Personal</span></h2>
									<p>Contamos con especialistas capacitados para cada &aacute;rea.</p>
									<a>View more</a>
								</figcaption>
							</figure>
						</div>
						<p> Director General: Diana Cogorno de Arias.<br>
							Coordinador, Gu&iacute;as, personal param&eacute;dico, de log&iacute;stica y de recreaci&oacute;n.<br>
								El personal es rigurosamente seleccionado y recibe talleres de capacitaci&oacute;n.<br>       
								Es contratado de acuerdo a la cantidad de ni&ntilde;os inscritos y sus edades.</p>
						<div class="btn-holder">
							<a href="../inscripciones" class="btn btn-link">Ir a inscripciones</a>
						</div>
					</div>
				</div>
			
			</div>
		</section>
		<?php include('../customize/section_footer.php') ?>
	</div>
	<script src="../js/jquery-1.11.2.min.js"></script>
	<script src="../js/bootstrap.js"></script>
	<script src="../js/jquery.main.js"></script>
	<script src="../js/classie.js"></script>
	<script src="../js/SmoothScrolling.js"></script>
</body>
</html>